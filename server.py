from flask import Flask, request
import sqlite3

app = Flask(__name__)

GET_KEY_FETCH = """SELECT name FROM keyboardCodes WHERE utf8 = ?"""
GET_CODE_FETCH = """SELECT utf8 FROM keyboardCodes WHERE name = ?"""


def fetch_db(fetch, data):
    conn = sqlite3.connect('keyboardCodes.db', isolation_level=None)
    cur = conn.cursor()
    value = cur.execute(fetch, [data]).fetchone()
    cur.close()
    return value


@app.route('/get_key', methods=['GET'])
def get_key():
    code = request.json['code']
    key = fetch_db(GET_KEY_FETCH, code)
    if key:
        return key[0], 200
    return 'None', 404


@app.route('/get_code', methods=['GET'])
def get_code():
    key = request.json['key']
    code = fetch_db(GET_CODE_FETCH, key)
    if code:
        return str(code[0]), 200
    return 'None', 404


if __name__ == '__main__':
    app.run(host='', port=8000, debug=True)
