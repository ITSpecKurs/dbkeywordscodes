from requests import get
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow
import sys

URL = 'http://127.0.0.1:8000/'
UI_NAME = "Window.ui"


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.win = uic.loadUi(UI_NAME, self)

        self.send.clicked.connect(self.push_code)

        self.shift = 0
        self.caps = 0

    def keyPressEvent(self, event):
        if event.key() == 16777248:
            self.shift = 1

        if event.key() == 16777252:
            self.caps = abs(self.caps - 1)

        if 0 <= event.key() <= 2000:
            key = chr(event.key())
            if self.shift == self.caps:
                key = key.lower()
            self.push_key(key)

    def keyReleaseEvent(self, event):
        if event.key() == 16777248:
            self.shift = 0

    def push_key(self, key):
        self.label_rewrite(ServerRequests().get_code(key))

    def push_code(self):
        code = self.code.text()

        try:
            code = int(code)
        except ValueError:
            self.label_rewrite('Неверное\nзначение')
            return

        self.label_rewrite(ServerRequests().get_key(code))

    def label_rewrite(self, text):
        if text == 'None':
            self.text.setText('Неверное\nзначение')
            return
        self.text.setText(text)


class ServerRequests(MainWindow):
    def get_code(self, key: str) -> str:
        return get(URL + 'get_code', json={'key': key}).text

    def get_key(self, code: int) -> str:
        return get(URL + 'get_key', json={'code': code}).text


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
